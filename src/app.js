require('dotenv').config()
const express = require('express');
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const morgan = require('morgan');
const jwt = require('json-web-token');

const app = express()
app.use(bodyParser.json())
// app.use(cookieParser)
var logger = morgan('combined')
app.use(logger)

// app.get('/', (req, res) => {
//    res.status(200).send()
// })

app.get('/test', (req, res) => {
    res.status(200).send({
        message: "Service Running."
    })
})

app.get('*', (req, res) => {
    let message = {
        status: "404",
        result: "Please make sure you entered the right path."
    }
    res.status('404').send(message);
})

app.listen(process.env.PORT, () => {
    // console.log(data)
    console.log('Listening on port: ' + process.env.PORT)
})